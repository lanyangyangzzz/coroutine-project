package com.szj.coroutine.project.bean

data class NetWorkBean(
    val code: Int = 0,
    val message: String = "",
    val result: Result = Result()
) {
    data class Result(
        val list: List<Child> = listOf(),
        val total: Int = 0
    ) {
        data class Child (
            val coverUrl: String = "",
            val duration: String = "",
            val id: Int = 0,
            val playUrl: String = "",
            val title: String = "",
            val userName: String = "",
            val userPic: String = ""
        )
    }
}
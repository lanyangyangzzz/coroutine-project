package com.szj.coroutine.project.jvm.blog3

import com.szj.coroutine.project.jvm.util.printlnJob
import com.szj.coroutine.project.jvm.util.printlnThread
import kotlinx.coroutines.*
import kotlin.concurrent.thread
import kotlin.coroutines.Continuation
import kotlin.coroutines.ContinuationInterceptor
import kotlin.system.measureTimeMillis

/*
 * 作者: 史大拿
 * 时间: 2023/2/13$ 20:24$
 */

/// TODO ======== first main ===================
//fun main() {
//    val useTime = measureTimeMillis {
//        runBlocking<Unit> {
//            printlnThread("main start") // TODO 代码1      ===调度前代码
//
//            val job = launch {
//                printlnThread("launch 1") // TODO 代码3    +++调度后代码
//            }
//            printlnJob("job = ", job) // TODO 代码5        ===调度前代码
//
//            val job2 = launch {
//                printlnThread("launch 2") // TODO 代码4    +++调度后代码
//            }
//
//            printlnJob("job2 = ", job2)  // TODO 代码6     ===调度前代码
//
//            printlnThread("main end") // TODO 代码2        ===调度前代码
//        }
//    }
//    println("runBlocking 执行时间为:$useTime")
//}

//
///// TODO ======== second IO ===================
//fun main() {
//    val useTime = measureTimeMillis {
//        runBlocking<Unit>(Dispatchers.IO) {
//            printlnThread("main start") // TODO 代码1
//
//            val job1 = launch {
//                printlnThread("launch 1") // TODO 代码3
//            }
//            printlnJob("IO job1 = ", job1)  // TODO 代码5
//
//            val job2 = launch {
//                printlnThread("launch 2") // TODO 代码4
//            }
//            printlnJob("IO job2 = ", job2) // TODO 代码6
//
//            printlnThread("main end") // TODO 代码2
//        }
//    }
//    println("runBlocking 执行时间为:$useTime")
//}
//
//
///// TODO ======== 案例1 ===================
fun main() {
    val useTime = measureTimeMillis {
        runBlocking<Unit> {
            printlnThread("main start") // TODO 代码1

            launch {
                printlnThread("launch 1") // TODO 代码3
            }
            delay(1000)
            launch {
                printlnThread("launch 2") // TODO 代码4
            }

            delay(2000)
            printlnThread("main end") // TODO 代码2
        }
    }
    println("runBlocking 执行时间为:$useTime")
}
//
//
///// TODO ======== 案例2 ===================
//fun main() {
//    val useTime = measureTimeMillis {
//        runBlocking<Unit> {
//            printlnThread("main start") // TODO 代码1
//
//            launch {
//                Thread.sleep(2000)
//                printlnThread("launch 1") // TODO 代码3
//            }
//            launch {
//                Thread.sleep(1000)
//                printlnThread("launch 2") // TODO 代码4
//            }
//
//            printlnThread("main end") // TODO 代码2
//        }
//    }
//    println("runBlocking 执行时间为:$useTime")
//}


/// TODO ======== 案例3 ===================
//fun main() {
//    println("main start")
//
//    runBlocking<Unit> {
//        println("runBlocking start")
//        delay(2000)
//        println("runBlocking end")
//    }
//
//    println("main 执行中..")
//
//    Thread.sleep(2100)
//    println("main end")
//}

//
///// TODO ======== 案例4 ===================
//fun main() = runBlocking<Unit> {
//
//    println("main start") // 1
//
//    val job = launch {
//        println("launch 1 start") // 2
//        delay(1000L)  // TODO 延时1
//        println("launch 1 end")  // 3
//    }
//
//    println("main mid")  // 4
//
//    val job2 = launch {
//        println("launch 2 start") //5
//        delay(1000L) // TODO 延时2
//        println("launch 2 end") //6
//    }
//
//    delay(1500) // TODO 延时3
//    println("main end") // 7
//    // 正确结果: 1,4,2,5,3,6,7
//}

///// TODO ======== 案例5 ===================
//fun main() = runBlocking<Unit> {
//
//    println("main start") // 1
//
//    val job = async { // TODO 协程1
//        println("launch 1 start")  // 2
//        delay(1000L)
//        println("launch 1 end")  // 3
//    }
//
//    println("main min")  // 4
//
//    job.join()
//
//    launch(job) {// TODO 协程2
//        println("launch 2 start") // 5
//        delay(1000L)
//        println("launch 2 end") // 6
//    }
//
//    println("main end") // 7
//    // 1,4,2,3,7
//}


///// TODO ======== 案例6 ===================
//fun main() = runBlocking<Unit> {// 父协程
//
//    println("main start") // 1
//
//    val job = launch {// TODO 协程1
//        println("1 start") // 2
//        delay(1000L)
//        println("launch 1 end") // 3
//    }
//
//    println("main mid") // 4
//
//    Thread.sleep(1000) // TODO 阻塞1
//    job.join()
////    printlnJob("coroutineContext = ", coroutineContext[Job]!!)
//    launch(coroutineContext + Dispatchers.IO) {// TODO 协程2
//        printlnThread("launch 2 start") // 5
//        delay(1000L)
//        printlnThread("launch 2 end") // 6
//    }
//
//    Thread.sleep(2000)// TODO 阻塞2
//    println("main end") // 7
//
//    // 正确答案:1,4,2,3,5,6,7
//}

// TODO ======== 案例7 ===================
//fun main() = runBlocking<Unit> {
//    println("main start") // 1
//
//    val job = launch(Dispatchers.IO) {// TODO 协程1
//        println("launch 1 start") // 2
//        delay(1000L)
//        println("launch 1 end") // 3
//    }
//
//    launch(job) {// TODO 协程2
//        println("launch 2 start") // 5
//        delay(1000L)
//        println("launch 2 end") // 6
//    }
//
//    println("main mid") // 7
//
//    Thread.sleep(500L) // TODO 阻塞1
//    job.cancel()
//
//    launch(job) {// TODO 协程3
//        println("launch 3 start") // 7
//        delay(1000L)
//        println("launch 3 end") // 8
//    }
//
//    println("main end") // 9
//}


// TODO ======== 案例8 ===================
//fun main() {
//    val useTime = measureTimeMillis {
//        runBlocking<Unit> {
//            println("main start") // 1
//
//            val job = GlobalScope.launch {// TODO 全局协程
//                println("launch 1 start") // 2
//                delay(1000L) // 延迟1
//                println("launch 1 end") // 3
//            }
//
//            println("main mid") // 4
//            launch(context = job) {// TODO 子协程
//                println("launch 2 start") // 5
//                delay(2000L) // 延迟2
//                println("launch 2 end") // 6
//            }
//
//            println("main end") // 7
//        }
//    }
//
//    println("使用时间: $useTime")
//}
package com.szj.coroutine.project.jvm.blog2

import com.szj.coroutine.project.jvm.util.printlnThread
import kotlinx.coroutines.*
import kotlin.coroutines.*

/*
 * 作者: 史大拿
 * 时间: 2023/2/10$ 16:27$
 */

// region CoroutineStart.ATOMIC
/*
 * 作者: 史大拿
 * 时间: 2023/2/13 10:51
 * TODO:
 */
fun main() = runBlocking<Unit> {
    printlnThread("1.main start")

    launch(Dispatchers.IO) {
        printlnThread("3.我是父协程")
        launch( // 会跟随父协程的线程状态而变化
            context = Dispatchers.IO + CoroutineName("测试"),
            start = CoroutineStart.UNDISPATCHED // 不进行调度
        ) {
            printlnThread("4.UN_DISPATCHED launch start")
            withContext(Dispatchers.IO) {// 切换线程
                printlnThread("5.withContext launch ")
            }
            printlnThread("6.UN_DISPATCHED launch end")
        }
    }
    printlnThread("2.main end")
}

// endregion

// region CoroutineStart.ATOMIC
/*
 * 作者: 史大拿
 * 时间: 2023/2/10 17:59
 */

//fun main() = runBlocking<Unit> {
//    printlnThread("main start")
//    // CoroutineStart.DEFAULT 默认就是这个
//    val job = launch(start = CoroutineStart.ATOMIC) {
//        printlnThread("launch atomic 11")
////        delay(1000_0000_0000) // 等待好久之后恢复协程执行
//        withContext(Dispatchers.IO) {
//            printlnThread("withContext IO")
//        }
//        printlnThread("launch atomic 22")
//    }
//
//    job.cancel()
//
//    printlnThread("main end")
//}
//// endregion


// region  CoroutineStart.LAZY
//
//fun main() = runBlocking<Unit> {
//
//    printlnThread("main ")
//    launch {
//        printlnThread("coroutine 2")
//    }
//    val job = launch(start = CoroutineStart.LAZY) {
//        printlnThread("launch 3 ")
//    }
//
//    launch {
//
//        printlnThread("coroutine 4")
//    }
//
//    job.start()
//
//    // job.join()
//}
// endregion

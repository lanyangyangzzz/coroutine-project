package com.szj.coroutine.project.jvm.blog5

import com.szj.coroutine.project.jvm.util.printlnThread
import kotlinx.coroutines.*
import kotlin.coroutines.Continuation
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.coroutines.startCoroutine

/*
 * 作者: 史大拿
 * 时间: 2023/2/23$ 09:51$
 */

//private suspend fun requestData1() = run {
//    delay(1000)
//    "请求数据1"
//}
//
//private suspend fun requestData2() = run {
//    delay(2000)
//    "请求数据2"
//}
//
//private suspend fun requestData3() = run {
//    delay(3000)
//    "请求数据3"
//}

fun main() {
//    val deprecated1 = async {
//        requestData1()
//    }
//    println(deprecated1)
//
//    val deprecated2 = async {
//        requestData2()
//    }
//    println(deprecated2)
//
//    val deprecated3 = async {
//        requestData3()
//    }
//    println(deprecated3)
    val scope = CoroutineScope(EmptyCoroutineContext)

    scope.launch {
        println("测试1")

        delay(1000)
        println("测试2")
        delay(2000)

        println("测试3")
        delay(3000)
    }
}


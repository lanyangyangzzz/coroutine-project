package com.szj.coroutine.project.jvm.util

import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.Job

/*
 * 作者: 史大拿
 * 时间: 2023/2/10$ 09:36$
 */

fun printlnThread(any: Any?) {
    println("$any \t thread:${Thread.currentThread().name}")
}

fun printlnJob(key:Any,job: Job) {
    println(
        "${key}\tjob状态:${if (job.isActive) "活跃" else "未活跃"}\t" +
                "${if (job.isCancelled) "取消" else "未取消"} \t" +
                if (job.isCompleted) "完成" else "未完成"
    )
}
fun<T> printlnCC(key:Any,cc: CancellableContinuation<T>) {
    println(
        "${key}\tCancellableContinuation状态:${if (cc.isActive) "活跃" else "未活跃"}\t" +
                "${if (cc.isCancelled) "取消" else "未取消"} \t" +
                if (cc.isCompleted) "完成" else "未完成"
    )
}
package com.szj.coroutine.project.jvm.blog2

import com.szj.coroutine.project.jvm.util.printlnThread
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/*
 * 作者: 史大拿
 * 时间: 2023/2/10$ 15:08$
 */
fun main() = runBlocking<Unit> {

    launch(context = CoroutineName("我是自定义的名字") + Dispatchers.IO) {
        printlnThread("launch1")
        launch {
            launch {
                printlnThread("launch2")

                launch {
                    launch(context = CoroutineName("我是launch3的名字")) {
                        printlnThread("launch3")
                    }
                }
            }
        }
    }
}